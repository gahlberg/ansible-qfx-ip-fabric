#!/usr/bin/python2
import yaml
import sys
from netaddr import IPNetwork
import os

"""

This scripts builds up the filter yaml file. If new vlans is created,
the filter file gets updated with the default rules.

It will also add rules created in the exceptions file

This is part of the Ansible automation of the IP fabric at the
meteorological institute of Norway.

Author: Erlend Roesok
Date: June 2018

"""

### Global Variables ###
playbook_dir = sys.argv
sys.path.append(playbook_dir)
vars_dir = playbook_dir[1] + '/group_vars/all/'
host_vars = playbook_dir[1] + '/host_vars/'
dirs = [d for d in os.listdir(host_vars) if os.path.isdir(os.path.join(host_vars))]
vlans = []
exceptions = []
met_subnet = '10.10.0.0/16'
rfc_1918 = ['10.0.0.0/8', '172.16.0.0/12', '193.168.0.0/16']


# Create a list containing dicts for leafs and their vlans
# Do the same for the leafs/cores and their exception rules
for folder in dirs:
    if 'leaf' in folder:
        try:
            leaf_vlans = {}
            leaf_exceptions = {}
            vlan_file = str(host_vars + folder + '/vlans.yaml')
            exception_file = str(host_vars + folder + '/exceptions.yaml')
            with open(vlan_file, 'r') as main:
                local_vlans = yaml.load(main)
                leaf_vlans.setdefault(folder, local_vlans)
                vlans.append(leaf_vlans)
            with open(exception_file, 'r') as main:
                local_exceptions = yaml.load(main)
                leaf_exceptions.setdefault(folder, local_exceptions)
                exceptions.append(leaf_exceptions)
        except:
            continue
    if 'core' in folder:
        core_vlans = {}
        core_exceptions = {}
        vxlan_file = str(vars_dir + 'vxlan.yaml')
        vxlan_exception_file = str(vars_dir + 'vxlan_exceptions.yaml')
        with open(vxlan_file, 'r') as main:
            vxlans_orig = yaml.load(main)
        vxlans_dict = {}
        vxlans = vxlans_dict.setdefault('vlans', {})
        internal = vxlans.setdefault('internal', [])
        external = vxlans.setdefault('external', [])
        for vxlan in vxlans_orig['vxlan']:
            if vxlan['type'] == 'internal':
                internal.append({'description': vxlan['description'],
                                 'subnet': {'vlan_id': vxlan['vlan_id'],
                                            'gateway': vxlan['gateway']}})
        core_vlans.setdefault(folder, vxlans_dict)
        vlans.append(core_vlans)
        with open(vxlan_exception_file, 'r') as main:
            vxlan_exceptions = yaml.load(main)
            if vxlan_exceptions:
                core_exceptions.setdefault(folder, core_exceptions)
                exceptions.append(core_exceptions)

#### Open the network yaml files ####
try:
    with open(vars_dir + 'networks.yaml', 'r') as f:
        networks = yaml.load(f)
except:
    sys.stderr('ERROR: Unable to open the file' + vars_dir + '/networks.yaml. Does it exist?')
try:
    with open(vars_dir + 'system.yaml', 'r') as f:
        system = yaml.load(f)
except:
    sys.stderr("ERROR: Unable to open the file {}/system.yaml. Does it exist?").format(vars_dir)

external_networks_v4 = networks['networks']['external']

#### Update networks with external cloud subnets ####
for leaf in vlans:
    name = leaf.keys()
    name = name[0]
    external = leaf[name]['vlans']['external']
    if external:
        for vlan in external:
            description = vlan['description']
            address4 = IPNetwork(vlan['subnet']['gateway'])
            subnet4 = address4.cidr
            address6 = IPNetwork(vlan['subnet']['gateway6'])
            subnet6 = address6.cidr
            if description not in external_networks_v4.keys():
                external_networks_v4.setdefault(description, str(subnet4))

#### Loop through all the leafs ####
for leaf in vlans:
    name = leaf.keys()
    name = name[0]
    internal = leaf[name]['vlans']['internal']
    external = leaf[name]['vlans']['external']
    firewall = {}
    firewall_filter = firewall.setdefault('filter', {})
    ipv4_filter = firewall_filter.setdefault('inet', [])

    # Populate Internal vlans
    if internal:
        for group in internal:
            description = group['description']
            vlan = {'name': description, 'rules': []}
            rules = vlan['rules']
            # ADD EXTERNAL NTP SERVERS
            primary_ntp = {"action": "allow",
                      "src": system['system']['ntp']['primary'],
                      "port": '123',
                      "proto": 'udp'}
            rules.append(primary_ntp)
            for secondary in system['system']['ntp']['secondary']:
                secondary_ntp = {"action": "allow",
                          "src": secondary,
                          "port": '123',
                          "proto": 'udp'}
                rules.append(secondary_ntp)
            # ADD CUSTOM RULES FROM EXCEPTION FILE
            for switch in exceptions:
                lname = switch.keys()
                lname = lname[0]
                if lname == name:
                    filter_list = switch[lname]['filter']['inet']
                    for item in filter_list:
                        if item['name'] == description:
                            for rule in item['rules']:
                                rules.append(rule)
            # BLOCK EXTERNAL NETWORKS
            for vlan_name, subnet in external_networks_v4.iteritems():
                           src = {"action": "reject",
                                     "src": str(subnet),
                                     "port": "all",
                                     "proto": "ip"}
                           rules.append(src)
           # ALLOW FROM MET-SUBNET
            src = {"action": "allow",
                      "src": met_subnet,
                      "port": 'all',
                      "proto": 'ip'}
            rules.append(src)
            # ALLOW FROM PRIVATE ADDRESS SPACE
            for subnet in rfc_1918:
                src = {"action": "allow",
                          "src": subnet,
                          "port": 'all',
                          "proto": 'ip'}
                rules.append(src)
            ipv4_filter.append(vlan)

    # Write to the firewall filter yaml file

    # Workaround if yaml dump start spitting out aliases, might be buggy.
    noalias_dumper = yaml.dumper.SafeDumper
    noalias_dumper.ignore_aliases = lambda self, data: True

    yaml_file = open(host_vars + name + '/filters.yaml', 'w')
    # Workaround if yaml dump start spitting out aliases, might be buggy.
    noalias_dumper = yaml.dumper.SafeDumper
    noalias_dumper.ignore_aliases = lambda self, data: True

    yaml_file = open(host_vars + name + '/filters.yaml', 'w')
    yaml_file.write(yaml.dump(firewall, default_flow_style=False))
    yaml_file.flush()
    yaml_file.close()
