# Ansible automation of QFX IP-Clos.

The following configuration is supported

* System configuration
* Linknets
* EBGP
* IBGP Overlay
* EVPN/VXLAN
* IRB interfaces
* DHCP relay
* Firewall filters
* Control Plane policing
* Access/Trunk/LACP ports

## Requirements

Follow the [documentation](https://www.juniper.net/documentation/en_US/junos-ansible/topics/task/installation/junos-ansible-server-installing.html) for Ansible Juniper.junos.

Run the playbook make_clean.yaml once to create build directories for configuration parts.

## Topology

The IP-Fabric consist of leafs, spines and cores. The leafs and spines are QFX5100 switches, while the cores are QFX10000 switches. The cores are collapsed such that each leaf has an uplink to all spines and cores. The cores participates in the EVPN/VXLAN overlay and acts at gateways for the vteps. Other than they will function as spines.

Scripts
--------

The firewall.py script is run to create the firewall filters for an internal vlan. It is important to run this script after adding a new vlan or when you have created any exception rules in the variable files vxlan_exception.yaml or exceptions.yaml. If the script is not run the firewall filters will not get updated and might pose a security risk.


Playbook Breakdown
-----------------

#### commit

Purpose: Build configuration for all leafs, spines and cores.
         Push configuration to the hosts. Automatically roll back configuration after a specified time.

Process:

**Config Leaf/Spine/Core --> Push Config**

Inludes: conf.yaml 

#### confirm

Purpose: Does a dummy commit in order to stop the configuration being rolled back.

#### conf.yaml

Purpose: Create configuration for all switches.

#### make_clean.yaml:

Purpose: Initialize build directories for the configration parts. Will remove any configuration that has been created previously by ansible roles.

#### fetch_config.yaml:

Purpose: Fetches the configuration from the devices.

Roles
-----

Each role refers to a top-level configuration of the juniper configuration syntax. If you want to create interface specific configuration you add it to the 'interface' role. 

Tasks
-----

In each role there is tasks that creates the configuration parts. Template extentions ending with *.core.j2 will be automatically created for the core switches. This is achived by using 'with_fileglob', see example below. If there is no extention this is most likely because it is run for all switches.

Example:

```yaml
- name: CORE INTERFACE CONFIGURATION
  when: inventory_hostname in groups['core']
  template:
    src: "{{ item }}"
    dest: "{{ build_dir }}/{{ item | basename }}.conf.part"
  with_fileglob:
    - "templates/*core.j2
```

###### Commit / Confirm

These tasks resides in the playbook commit.yaml and confirm.yaml respectively.

commit.yaml:

Uses [junos_install_config](http://junos-ansible-modules.readthedocs.io/en/1.3.1/junos_install_config.html) from the Juniper.junos module to push the configuration file to the switch over netconf.
It has been configured with the options confirm and overwrite. The confirm option ensures that the configuration will be automatically rolled back after specified number of minutes. The overwrite option makes the switch overwrite the entire configuration with the new configuration file. 

One import fact about the overwrite option. We use this option in order to make the configuration completely dynamic to any change. In this way we ensure consistensy of configuration and the ability to add and remove any part of the configuration. The back side is that it will not be possible to configure any of the switches manually as any change would be overwritten next time ansible makes a change.

confirm.yaml: 

Uses [junos_commit](http://junos-ansible-modules.readthedocs.io/en/1.3.1/junos_commit.html) from the Juniper.junos module to send commit to the switch. Run this playbook after running commit.yaml if you can confirm that the change has had the desired effect. This playbook will abort the automatic rollback.

Global Variables
----------------

#### Build-Config folder definitions

variable file: group_vars/all/build.yaml

contains folder definitions used by the build-config role.

Example:

```yaml
tmp_dir: /tmp/build
build_dir: "{{ tmp_dir }}/{{ inventory_hostname }}/tmp"
junos_conf: "config/{{ inventory_hostname }}.conf"
```

#### Overlay 

variable file: group_vars/all/overlay.yaml

Contains variables used to create the overlay for VXLAN/EVPN.

Example:

```yaml
overlay:
  asn: 60001
  vni_community: 10003
  vrf:
  - community_target: "10010:1"
    name: example_vrf
    route_distinguisher: 2010
```

#### Common Variables

variable file: group_vars/all/common.yaml

Contains variables used by all devices such as MTU, Global Prefixes and BFD.

Example:

```yaml
mtu:
  physical: 9192
  logical: 9000
networks:
  loopbacks4: '10.10.199.0/24'
  loopbacks6: 'fd1d:7a06:79ce:f0ce:199::/64'
bfd:
  min_interval: 350
  multiplier: 3
  mode: automatic
```

#### Network Definitions

variable file: group_vars/all/networks.yaml

list of networks not found in the IP-fabric. Admin networks are used for access control, external networks are used by firewall filters.

Example:

```yaml
networks:
    admin:
        admin_users: "10.10.20.0/24" 
    external:
        dmz: "10.20.10.0/24" 
```

#### Secrets

variable file: group_vars/all/secrets.yaml

secrets for local user accounts.

Example: 

```yaml
secret:
    root: < enrypted string >
    diag: < enrypted string >
    radius: < enrypted string >
```
#### System Variables

variable file: group_vars/all/system.yaml

system specific variables like DNS/NTP/SNMP servers etc.

Example: 

```yaml
system:
    dns:
      - 10.12.10.2
      - 10.12.10.3
    radius:
      - 10.21.10.1
    syslog:
      - 10.20.1.10
    ntp:
        primary: 10.20.1.5
        secondary:
          - 10.20.1.2
    snmp: 
        servers:
          - 10.10.10.1
        users:
          - name: librenms
            sha_encrypted: < encrypted string > 
            aes_encrypted: < encrypted string > 

```

#### User Varibles

variable file: group_vars/all/users.yaml

all non local user accounts and their public keys.

Example:

```yaml
keys:
  - user: admin_user
    rsa: < public key > 
```

#### VXLAN firewall exception

variable file: group_vars/all/vxlan_exceptions.yaml

This contains the user specified firewall rules. Rules entered here will be placed on the firewall ruleset before the rules that deny. See documentation for filters below.

Example:

```yaml
filter:
    inet:
      - name: example_vxlan
        rules:
        - action: allow
          port: '80'
          proto: 'tcp'
          src: 10.10.32.20
          dst: 10.20.20.10
```
Optional: 'dst'

This will allow traffic from 10.10.32.20 on port 80 to 10.20.20.10. If dst is not specified the destination will be the subnet of the vlan.


#### VXLAN variables

variable file: group_vars/all/vxlan.yaml

all vxlan configuration contains a description, vlan_id, active/passive router and their gateway ip, a virtual gateway ip, the vrf it belongs to and if it is internal or external.

Example:

```yaml
vxlan:
  - description: example_vxlan
    vlan_id: 123
    active_router: lab-core1
    passive_router: lab-core2
    gateway: "10.10.25.2"
    secondary_gateway: "10.10.25.3/21"
    virtual_gateway: "10.10.25.1"
    vrf: example_vrf
    type: internal
```

Host Variables
----------------

#### Filters

variable file: host_vars/host_name/filters.yaml

These variables gets created automatically by scripts/firewall.py.

These are the rules that gets created:

1. Permit TCP-Established (hard-coded into template)
2. Permit ICMP-Reply (hard-coded into template)
3. Add rules from exception file
3. Deny Traffic from External Networks inside our own prefix (uses networks.yaml and vlans.yaml)
4. Permit Traffic from our own prefix (10.10.0.0/16)
5. Permits Traffic from RFC-1918 (10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16)
6. Implicit Deny

Only Internal vlans will get filters. External networks are fully open.

#### Firewall filter exceptions

variable file: host_vars/host_name/exceptions.yaml

This contains the user specified firewall rules. Rules entered here will be placed on the firewall ruleset before the rules that deny our external subnets. See filters above.

Example:
```yaml
filter:
    inet:
      - name: example_vlan
        rules:
        - action: allow
          port: '80'
          proto: 'tcp'
          src: 10.10.32.20
          dst: 10.20.20.10
```
Optional: 'dst'

This will allow traffic from 10.10.32.20 on port 80 to 10.20.20.10. If dst is not specified the destination will be the subnet of the vlan.

#### Port variables

variable file: host_vars/host_name/ports.yaml

This contains all access, trunk and aggregated port variables.

Access-port example:

```yaml
- description: example_server
  interface: xe-0/0/7:1
  enabled: False
  tagged: False
  untagged_vlan: example_vlan
```

Trunk-port example:

```yaml
- description: example_server
  interface: xe-0/0/7:1
  enabled: False
  tagged: True
  native_vlan: example_vlan1
  tagged_vlan: "[ example_vlan1, example_vlan2 ]"
```

optional: native_vlan

Aggregated port example:

```yaml
- description: example_server
  interface: ae0
  enabled: True
  tagged: True
  native_vlan: example_vlan1
  tagged_vlan: "[ example_vlan1, example_vlan2 ]"

- description: example_server
  interface: xe-0/0/2:1
  enabled: True
  lag: ae0

- description: example_server
  interface: xe-0/0/2:2
  enabled: True
  lag: ae0
```

#### Underlay Variables

variable file: host_vars/host_name/underlay.yaml

Contains variables used to configure uplinks and underlay bgp configuration. Also contains the management interface IP.

Note that we specify the peer on the interface. This is required in order to configure the BGP neighbors.

Example:

```yaml
loopback_ip4: '10.10.195.193/32'
loopback_ip6: 'fd1d:7a06:79ce:f0ce:199::193/128'

underlay:
    asn: 64700
    mgmt: "10.254.1.56/24"
    interfaces:
      - name: et-0/0/0
        peer: lab-core1
        inet: "10.10.195.1/31"
        inet6: "fd1d:7a06:79ce:f0ce:f100::2/126"
      - name: et-0/0/1
        peer: lab-core2
        inet: "10.10.195.17/31"
        inet6: "fd1d:7a06:79ce:f0ce:f101::2/126"
      - name: et-0/0/2
        peer: spine-bi
        inet: "10.10.195.33/31"
        inet6: "fd1d:7a06:79ce:f0ce:f102::2/126"
      - name: et-0/0/3
        peer: spine-bj
        inet: "10.10.195.49/31"
        inet6: "fd1d:7a06:79ce:f0ce:f103::2/12
```

#### Vlans

variable file: host_vars/host_name/vlans.yaml

Contains all vlan variable. Used to configure layer3 and layer2 vlans. They are group into internal and external vlans. Internal vlans.

Example:

```yaml
vlans:
    internal:
      - description: internal_example_vlan
        subnet:
            vlan_id: 111
            gateway: "10.10.111.1/24"
            gateway6: "fd1d:7a06:79ce:f0ce:111::1/64"
            acl_enabled: True
    external:
      - description: external_example_vlan
        subnet:
            vlan_id: 222
            gateway: "10.10.222.0/24"
            gateway6: "fd1d:7a06:79ce:f0ce:222::1/64"
            acl_enabled: True
```
